import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/orders_provider.dart' show OrdersProvider;
import 'package:shop_app/widgets/app_drawer.dart';
import 'package:shop_app/widgets/error_dialog.dart';

import '../widgets/order_item.dart';

class OrdersScreen extends StatelessWidget {
  static const String routeName = '/orders';

  const OrdersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: AppDrawer(),
        appBar: AppBar(
          title: Text('Your Orders'),
        ),

        // Note: FutureBuilder should be store outside of build function in stateful widget
        // because we don't want it to rebuild everytime
        body: FutureBuilder(
          future: Provider.of<OrdersProvider>(context, listen: false)
              .fetchAndSetOrders(),
          builder: (ctx, dataSnapShot) {
            if (dataSnapShot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            }
            if (dataSnapShot.error != null) {
              // TODO: Handle Error
              return Center(child: Text("Error Occured"));
            } else {
              return Consumer<OrdersProvider>(
                  builder: (ctx, orderData, child) => ListView.builder(
                        itemBuilder: (ctx, idx) {
                          final order = orderData.orders[idx];
                          return OrderItem(order);
                        },
                        itemCount: orderData.orders.length,
                      ));
            }
          },
        ));
  }
}
