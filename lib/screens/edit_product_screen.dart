import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'package:shop_app/widgets/error_dialog.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = '/edit-product';

  EditProductScreen({Key? key}) : super(key: key);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _InitValues {
  String title;
  String description;
  String price;
  String imageURL;

  _InitValues(this.title, this.description, this.price, this.imageURL);
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();

  bool _isInit = false;
  final _initVal = _InitValues('', '', '', '');

  bool _isLoading = false;

  Product _editedProduct = Product(
    id: 0,
    title: '',
    description: '',
    price: 0,
    imageUrl: '',
    authHeader: {'': ''},
  );

  @override
  void initState() {
    _imageUrlFocusNode.addListener(_updateImageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final productId = ModalRoute.of(context)!.settings.arguments;
      if (productId != null) {
        _editedProduct = Provider.of<ProductsProvider>(context, listen: false)
            .findById(productId as int);
        _initVal.title = _editedProduct.title;
        _initVal.description = _editedProduct.description;
        _initVal.price = _editedProduct.price.toString();
        // _initVal.imageURL = _editedProduct.imageUrl; // We can't set initval and controller at the same time
        _imageUrlController.text = _editedProduct.imageUrl;
      }
    }
    _isInit = true;
    super.didChangeDependencies();
  }

  // Dispose Focus to prevent memory leak
  @override
  void dispose() {
    _priceFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.removeListener(_updateImageUrl);
    _imageUrlFocusNode.dispose();
    super.dispose();
  }

  void _updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      // Create a check logic same as the image URL validator
      setState(() {});
    }
  }

  void _saveForm() async {
    final isValid = _form.currentState!.validate();
    if (isValid) {
      _form.currentState!.save();
      setState(() {
        _isLoading = true;
      });
      print(_editedProduct.id);
      try {
        if (_editedProduct.id == 0) {
          await Provider.of<ProductsProvider>(context, listen: false)
              .addProduct(_editedProduct);
        } else {
          await Provider.of<ProductsProvider>(context, listen: false)
              .updateProduct(_editedProduct.id, _editedProduct);
        }
      } catch (err) {
        await showDialog<Null>(
            context: context,
            builder: (ctx) => ErrorDialog(() {
                  Navigator.of(context).pop();
                }));
      }
      // finally {
      //   // stop loading and pop the screen
      //   setState(() {
      //     _isLoading = false;
      //   });
      //   Navigator.of(context).pop();
      // }

      // stop loading and pop the screen
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pop();

      // Navigator.of(context).pop();
    } else {
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
        actions: [
          IconButton(onPressed: _saveForm, icon: Icon(Icons.save_sharp))
        ],
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : Padding(
              padding: const EdgeInsets.all(15),
              child: Form(
                key: _form,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      TextFormField(
                        initialValue: _initVal.title,
                        decoration: InputDecoration(labelText: 'Title'),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          // Not that neccessary, the program will automatically navigate to the next node
                          FocusScope.of(context).requestFocus(_priceFocusNode);
                        },
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'Please provide title';
                          }
                          return null;
                        },
                        onSaved: (val) {
                          _editedProduct = Product(
                            id: _editedProduct.id,
                            title: val!,
                            description: _editedProduct.description,
                            price: _editedProduct.price,
                            isFavorite: _editedProduct.isFavorite,
                            imageUrl: _editedProduct.imageUrl,
                            authHeader: _editedProduct.authHeader,
                          );
                        },
                      ),
                      TextFormField(
                        initialValue: _initVal.price,
                        decoration: InputDecoration(labelText: 'Price'),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        focusNode: _priceFocusNode,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'Please provide price';
                          } else if (num.tryParse(val) == null) {
                            return 'Please enter numeric';
                          } else if (num.tryParse(val)! < 0) {
                            return 'Price must greater than 0';
                          } else {
                            return null;
                          }
                        },
                        onSaved: (val) {
                          _editedProduct = Product(
                            id: _editedProduct.id,
                            title: _editedProduct.title,
                            description: _editedProduct.description,
                            price: double.parse(val!),
                            isFavorite: _editedProduct.isFavorite,
                            imageUrl: _editedProduct.imageUrl,
                            authHeader: _editedProduct.authHeader,
                          );
                        },
                      ),
                      TextFormField(
                        initialValue: _initVal.description,
                        decoration: InputDecoration(labelText: 'Description'),
                        maxLines: 3,
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.multiline,
                        validator: (val) {
                          if (val!.isEmpty) {
                            return 'Please provide description';
                          } else if (val.length < 10) {
                            return 'Description must be longer than 10';
                          }
                          return null;
                        },
                        onSaved: (val) {
                          _editedProduct = Product(
                            id: _editedProduct.id,
                            title: _editedProduct.title,
                            description: val!,
                            price: _editedProduct.price,
                            imageUrl: _editedProduct.imageUrl,
                            isFavorite: _editedProduct.isFavorite,
                            authHeader: _editedProduct.authHeader,
                          );
                        },
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            width: 100,
                            height: 100,
                            margin: EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                                border: Border.all(
                              width: 1,
                              color: Colors.grey,
                            )),
                            child: _imageUrlController.text.isEmpty
                                ? Text('Enter URL')
                                : FittedBox(
                                    child: Image.network(
                                      _imageUrlController.text,
                                      fit: BoxFit.cover,
                                      loadingBuilder:
                                          (context, child, loadingProgress) =>
                                              (loadingProgress == null)
                                                  ? child
                                                  : CircularProgressIndicator(),
                                      errorBuilder: (ctx, error, stackTrace) =>
                                          Text('Invalid Image URL'),
                                    ),
                                  ),
                          ),
                          Expanded(
                            child: TextFormField(
                              // initialValue: _initVal.imageURL, // Init Value can't be set with controller
                              decoration:
                                  InputDecoration(labelText: 'Image URL'),
                              keyboardType: TextInputType.url,
                              textInputAction: TextInputAction.done,
                              controller: _imageUrlController,
                              focusNode: _imageUrlFocusNode,
                              // onEditingComplete: () {
                              //   setState(() {});
                              // },
                              validator: (val) {
                                if (val!.isEmpty) {
                                  return 'Image URL can\'t be empty';
                                }
                                if (!val.startsWith('http') &
                                    (!val.startsWith('https'))) {
                                  return 'Please enter a valid URL';
                                }
                                if (!val.endsWith('.png') &&
                                    !val.endsWith('.jpg')) {
                                  return 'Please enter a valid link';
                                }
                                return null;
                              },
                              onSaved: (val) {
                                _editedProduct = Product(
                                  id: _editedProduct.id,
                                  title: _editedProduct.title,
                                  description: _editedProduct.description,
                                  price: _editedProduct.price,
                                  isFavorite: _editedProduct.isFavorite,
                                  imageUrl: val!,
                                  authHeader: _editedProduct.authHeader,
                                );
                              },
                              onFieldSubmitted: (_) => _saveForm(),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
