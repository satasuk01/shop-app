import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:shop_app/const.dart';
import 'package:shop_app/models/http_exception.dart';

import 'product.dart';

class ProductsProvider with ChangeNotifier {
  final log = Logger('ProductsProvider');
  final Map<String, String>? authHeader;
  List<Product> _items = [];

  ProductsProvider(this.authHeader, this._items);
  // List<Product> _items = [
  //   Product(
  //     id: 'p1',
  //     title: 'Red Shirt',
  //     description: 'A red shirt - it is pretty red!',
  //     price: 29.99,
  //     imageUrl:
  //         'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
  //   ),
  //   Product(
  //     id: 'p2',
  //     title: 'Trousers',
  //     description: 'A nice pair of trousers.',
  //     price: 59.99,
  //     imageUrl:
  //         'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
  //   ),
  //   Product(
  //     id: 'p3',
  //     title: 'Yellow Scarf',
  //     description: 'Warm and cozy - exactly what you need for the winter.',
  //     price: 19.99,
  //     imageUrl:
  //         'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
  //   ),
  //   Product(
  //     id: 'p4',
  //     title: 'A Pan',
  //     description: 'Prepare any meal you want.',
  //     price: 49.99,
  //     imageUrl:
  //         'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
  //   ),
  // ];

  // var _showFavoriteOnly = false;

  // final Map<String, String> authHeader;
  // ProductsProvider(this.authHeader);

  List<Product> get items {
    // if (_showFavoriteOnly) {
    //   return _items.where((element) => element.isFavorite).toList();
    // }
    // Return a copy of _items
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((element) => element.isFavorite).toList();
  }

  Product findById(int id) {
    return _items.firstWhere((element) => element.id == id);
  }

  Future<void> deleteProduct(int id) async {
    // final url = Uri.parse('http://127.0.0.1:8000/products/remove/$id');
    final url = Uri.parse(RouterConst.URL + '/products/remove/$id');
    log.fine('DELETE: $id');

    final existingProductIdx = _items.indexWhere((element) => element.id == id);
    Product? existingProduct = _items[existingProductIdx];

    // _items.removeWhere((element) => element.id == id);
    _items.removeAt(
        existingProductIdx); // Remove from the list but still keep in memory
    notifyListeners();
    try {
      final res = await http.delete(url);
      if (res.statusCode >= 400) {
        // Roll back operation
        // log.shout(err.toString());
        _items.insert(existingProductIdx, existingProduct);
        notifyListeners();
        throw HttpException(
            'Could not delete a product. ' + json.decode(res.body).toString());
      }
    } catch (err) {
      throw err;
    }
    existingProduct = null;
  }

  Future<void> updateProduct(int id, Product newProd) async {
    final prodIdx = _items.indexWhere((element) => element.id == id);
    if (prodIdx > 0) {
      // final url = Uri.parse('http://127.0.0.1:8000/products/update/$id');
      final url = Uri.parse(RouterConst.URL + '/products/update/$id');

      try {
        final res = await http.put(
          url,
          body: json.encode({
            'title': newProd.title,
            'description': newProd.description,
            'price': newProd.price,
            'image_url': newProd.imageUrl,
          }),
        );

        log.info('UPDATE: ' + json.decode(res.body).toString());
      } catch (err) {
        log.severe("Products can't be updated!: ", err);
        throw err;
      }

      _items[prodIdx] = newProd;
      notifyListeners();
    } else {
      print('Error no such product Id');
    }
  }

  Future<void> fetchAndSetProducts([bool myProductFilter = false]) async {
    // var url = Uri.parse('http://127.0.0.1:8000/products/all');
    var url = Uri.parse(RouterConst.URL + '/products/all');
    if (myProductFilter) {
      // url = Uri.parse('http://127.0.0.1:8000/products/my-products');
      url = Uri.parse(RouterConst.URL + '/products/my-products');
    }
    // final favUrl = Uri.parse('http://127.0.0.1:8000/products/my-favorites');
    final favUrl = Uri.parse(RouterConst.URL + '/products/my-favorites');
    try {
      final res = await http.get(url, headers: authHeader);
      // print(json.decode(res.body).toString());
      final extractedData = json.decode(res.body) as List<dynamic>;

      // Get Favorite
      final favoriteResponse = await http.get(favUrl, headers: authHeader);
      log.info(favoriteResponse.statusCode);
      log.info(json.decode(favoriteResponse.body).toString());
      // TODO handle error
      final favoriteData =
          json.decode(favoriteResponse.body) as Map<String, dynamic>;
      final favorites = favoriteData['favorites'] as List;
      log.info(extractedData.toString());

      final List<Product> loadedProducts = [];
      extractedData.forEach((prodData) {
        loadedProducts.insert(
            0,
            Product(
              id: prodData['id'],
              title: prodData['title'],
              description: prodData['description'],
              price: prodData['price'],
              // isFavorite: prodData['is_favorite'],
              isFavorite: favorites.contains(prodData['id']),
              imageUrl: prodData['image_url'],
              authHeader: authHeader,
            ));
      });
      log.info('OK2');
      _items = loadedProducts;
      log.info('Products fetched!');
      notifyListeners();
    } catch (err) {
      log.severe("Products can't be fetched!: ", err.toString());
      throw err;
    }
  }

  Future<void> addProduct(Product product) async {
    // Post http
    // final url = Uri.parse('http://127.0.0.1:8000/products/add');
    final url = Uri.parse(RouterConst.URL + '/products/add');
    try {
      final res = await http.post(
        url,
        headers: authHeader,
        body: json.encode({
          'title': product.title,
          'description': product.description,
          'image_url': product.imageUrl,
          'price': product.price,
          'is_favorite': false,
        }),
      );

      print(json.decode(res.body));

      if (res.statusCode >= 400) {
        throw HttpException('Could not create a product.');
      }
      // Add new product when post complete
      final newProduct = Product(
        id: json.decode(res.body)['id'],
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
        authHeader: authHeader,
      );
      _items.insert(0, newProduct);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  // void showFavoriteOnly() {
  //   _showFavoriteOnly = true;
  //   notifyListeners();
  // }

  // void showAll() {
  //   _showFavoriteOnly = false;
  //   notifyListeners();
  // }
}
