import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:shop_app/models/http_exception.dart';

import 'package:shop_app/const.dart';
import './cart_provider.dart';

class OrderItem {
  final int id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    required this.id,
    required this.amount,
    required this.products,
    required this.dateTime,
  });
}

class OrdersProvider with ChangeNotifier {
  final Map<String, String>? authHeader;
  OrdersProvider(this.authHeader, this._orders);

  final log = Logger('ProductsProvider');
  List<OrderItem> _orders = [];

  List<OrderItem> get orders {
    return [..._orders];
  }

  Future<void> fetchAndSetOrders() async {
    // final url = Uri.parse('http://127.0.0.1:8000/orders/all');
    // final url = Uri.parse('http://127.0.0.1:8000/orders/my-orders');
    final url = Uri.parse(RouterConst.URL + '/orders/my-orders');
    try {
      final response = await http.get(url, headers: authHeader);
      // print(json.decode(response.body));
      if (response.statusCode >= 400) {
        throw HttpException("Can't fetch Order");
      }
      final List<OrderItem> loadedOrders = [];
      log.info(json.decode(response.body).toString());
      final extractedData = json.decode(response.body) as List<dynamic>;
      extractedData.forEach((orderData) {
        loadedOrders.add(OrderItem(
          id: int.parse(orderData['id']),
          amount: orderData['amount'],
          products: (orderData['order_details'] as List<dynamic>)
              .map((item) => CartItem(
                    id: item['item_id'],
                    title: item['title'],
                    quantity: item['quantity'],
                    price: item['price'],
                  ))
              .toList(),
          dateTime: DateTime.parse(orderData['date_time']),
        ));
      });
      _orders = loadedOrders;
      log.info('Order fetched!');
      notifyListeners();
    } catch (err) {
      log.shout('fetchAndSetOrders: ' + err.toString());
      throw err;
    }
  }

  Future<void> addOrder(List<CartItem> cartProducts, double total) async {
    // final url = Uri.parse('http://127.0.0.1:8000/orders/create');
    final url = Uri.parse(RouterConst.URL + '/orders/create');
    final timestamp = DateTime.now();

    try {
      final res = await http.post(url,
          headers: authHeader!,
          body: json.encode({
            'amount': total,
            'date_time': timestamp.toIso8601String(),
            'products': cartProducts
                .map((cp) => {
                      'id': cp.id,
                      'title': cp.title,
                      'quantity': cp.quantity,
                      'price': cp.price,
                    })
                .toList(),
          }));
      if (res.statusCode >= 400) {
        log.shout(json.decode(res.body).toString());
        throw HttpException("Can't add Order");
      }
      _orders.insert(
        0,
        OrderItem(
          id: json.decode(res.body)['id'],
          amount: total,
          products: cartProducts,
          dateTime: DateTime.now(),
        ),
      );
      notifyListeners();
    } catch (err) {
      throw err;
    }
  }
}
