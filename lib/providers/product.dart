import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/const.dart';
import 'package:shop_app/models/http_exception.dart';

class Product with ChangeNotifier {
  final int id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  final Map<String, String>? authHeader;
  bool isFavorite;

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
    this.isFavorite = false,
    required this.authHeader,
  });

  void _setFavValue(val) {
    isFavorite = val;
    notifyListeners();
  }

  Future<void> toggleFavoriteStatus() async {
    final oldStatus = isFavorite;

    _setFavValue(!isFavorite);

    // final url = Uri.parse('http://127.0.0.1:8000/products/update-favorite/$id');
    final url = Uri.parse(RouterConst.URL + '/products/update-favorite/$id');

    try {
      final res = await http.put(
        url,
        body: json.encode({'is_favorite': isFavorite}),
        headers: authHeader,
      );
      if (res.statusCode >= 400) {
        throw HttpException('Could not toggle favorite.');
      }
    } catch (err) {
      _setFavValue(oldStatus);
      throw err;
    }
  }
}
