import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop_app/const.dart';
import 'dart:convert';

import 'package:shop_app/models/http_exception.dart';

class AuthProvider with ChangeNotifier {
  String? _token;
  DateTime? _expiryDate;
  int? _userId;
  Timer? _authTimer;

  bool get isAuth {
    return token != null;
  }

  Map<String, String> get header {
    return {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${token ?? ''}',
    };
  }

  int? get userId {
    return _userId;
  }

  String? get token {
    if (_expiryDate != null &&
        _expiryDate!.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  final log = Logger('AuthProvider');

  Future<void> signup(String email, String password) async {
    return _authenticate(email, password, true);
  }

  Future<void> _authenticate(
    String email,
    String password,
    bool isRegister,
  ) async {
    // final urlRegister = Uri.parse('http://127.0.0.1:8000/users');
    final urlRegister = Uri.parse(RouterConst.URL + '/users');
    // final urlLogin = Uri.parse('http://127.0.0.1:8000/login');
    final urlLogin = Uri.parse(RouterConst.URL + '/login');
    try {
      if (isRegister) {
        final res = await http.post(urlRegister,
            body: json.encode({
              "name": "Test",
              "email": email,
              "password": password,
            }));

        log.shout(json.decode(res.body));
        if (res.statusCode >= 400) {
          throw HttpException(json.decode(res.body)['detail']);
        }
      }
      final res = await http.post(urlLogin, headers: {
        'Authorization': 'Bearer',
      }, body: {
        "grant_type": "",
        "scope": "",
        "client_id": "",
        "client_secret": "",
        "username": email,
        "password": password,
      });

      log.shout(res.statusCode.toString());
      log.shout(json.decode(res.body));
      if (res.statusCode >= 400) {
        throw HttpException(json.decode(res.body)['detail']);
      }

      // Set Token
      final body = json.decode(res.body);
      _token = body['access_token'];
      _userId = body['user_id'];
      _expiryDate = DateTime.parse(body['expire']).toLocal();
      _autoLogout();
      notifyListeners();

      log.info('Expiry Date: ' +
          DateTime.parse(body['expire']).toLocal().toString());

      // Store Login Data
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode({
        'token': _token,
        'userId': _userId,
        // 'expiryDate': _expiryDate != null
        //     ? _expiryDate!.toIso8601String()
        //     : DateTime.now().toIso8601String(),
        'expiryDate': _expiryDate!.toIso8601String(),
      });
      log.info('saved pref: ' + userData);
      final result = await prefs.setString('userData', userData);
      log.shout('Save Prefs: ' + result.toString());
    } catch (err) {
      log.shout('authentication: ' + err.toString());
      throw err;
    }
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, false);
  }

  Future<bool> tryAutoLogin() async {
    log.info('loading prefs for auto login');
    final prefs = await SharedPreferences.getInstance();

    if (!prefs.containsKey('userData')) {
      log.info('No prefs for auto login found');
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString('userData')!) as Map<String, dynamic>;
    final expiryDate =
        DateTime.parse(extractedUserData['expiryDate'] as String);
    if (expiryDate.isBefore(DateTime.now())) {
      log.info('Token is expire ' +
          DateTime.now().toString() +
          ' ' +
          expiryDate.toString());
      return false;
    }

    // Set up data
    _token = extractedUserData['token'] as String;
    _userId = extractedUserData['userId'] as int;
    _expiryDate = expiryDate;
    _autoLogout();
    log.info('AutoLogin ' + _userId.toString());

    return true;
  }

  Future<void> logout() async {
    _token = null;
    _expiryDate = null;
    _userId = null;
    if (_authTimer != null) {
      _authTimer!.cancel();
      _authTimer = null;
    }

    final prefs = await SharedPreferences.getInstance();
    // prefs.remove('userData');
    prefs.clear();

    log.info('logged out');
    notifyListeners();
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer!.cancel();
    }
    final expire = _expiryDate ?? DateTime.now();
    final timeToExpiry = expire.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
