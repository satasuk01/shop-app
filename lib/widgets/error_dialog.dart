import 'package:flutter/material.dart';

class ErrorDialog extends StatelessWidget {
  final Function onPress;
  const ErrorDialog(this.onPress);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('An error occured!'),
      // content: Text(err.toString()),
      content: Text('Something went wrong.'),
      actions: [
        TextButton(
          onPressed: () => onPress(),
          child: Text('Okay'),
        )
      ],
    );
  }
}
