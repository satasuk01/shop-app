import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import 'package:shop_app/screens/edit_product_screen.dart';

class UserProductItem extends StatelessWidget {
  final int id;
  final String title;
  final String imageUrl;

  const UserProductItem(this.id, this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    // for store context before delete
    final scaffoldMess = ScaffoldMessenger.of(context);

    final _cfDialog = DialogRoute(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('Are you sure?'),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text('No'),
          ),
          TextButton(
            onPressed: () async {
              try {
                await Provider.of<ProductsProvider>(context, listen: false)
                    .deleteProduct(id);
              } catch (err) {
                // Use Stored Scaffold for prevent when the product is deleted and the context changed
                scaffoldMess
                    .showSnackBar(SnackBar(content: Text('Deleting failed!')));
              }
              Navigator.of(context).pop();
            },
            child: Text('Yes'),
          ),
        ],
      ),
    );

    return ListTile(
      title: Text(title),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(imageUrl),
      ),
      trailing: Container(
        width: 100,
        child: Row(
          children: [
            IconButton(
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(EditProductScreen.routeName, arguments: id);
              },
              icon: Icon(Icons.edit),
              color: Theme.of(context).primaryColor,
            ),
            IconButton(
              onPressed: () {
                // Provider.of<ProductsProvider>(context, listen: false)
                //     .deleteProduct(id);
                Navigator.of(context).restorablePush((_, _2) => _cfDialog);
              },
              icon: Icon(
                Icons.delete,
                color: Theme.of(context).errorColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
