import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/cart_provider.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/screens/product_detail_screen.dart';

class ProductItem extends StatelessWidget {
  // final String id;
  // final String title;
  // final String imageUrl;

  // const ProductItem(this.id, this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<CartProvider>(context, listen: false);

    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: GridTile(
        child: MouseRegion(
          cursor: SystemMouseCursors.click,
          child: InkWell(
            onTap: () {
              Navigator.of(context).pushNamed(ProductDetailScreen.routeName,
                  arguments: product.id);
            },
            child: Hero(
              tag: product.id,
              child: FadeInImage(
                placeholder:
                    AssetImage('assets/images/product-placeholder.png'),
                imageErrorBuilder: (ctx, exception, stack) {
                  return Center(
                    child: Text(
                      "Image can't be loaded",
                    ),
                  );
                },
                fit: BoxFit.cover,
                image: NetworkImage(
                  product.imageUrl,
                  // errorBuilder: (ctx, exception, stack) {
                  //   return Center(
                  //     child: Text(
                  //       "Image can't be loaded",
                  //     ),
                  //   );
                  // },
                ),
              ),
            ),
          ),
        ),
        footer: GridTileBar(
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          // Animated Button: https://www.youtube.com/watch?v=l0VccGP-ygA
          // https://pub.dev/packages/like_button
          backgroundColor: Colors.black54,

          // Consumer wraps only part that we want to rebuild like we just wrap in fav button
          // This widget will be rebuilt only to Icon
          leading: Consumer<Product>(
            builder: (ctx, product, child) => IconButton(
              icon: product.isFavorite
                  ? Icon(
                      Icons.favorite,
                      color: Colors.pink.shade200,
                    )
                  : Icon(
                      Icons.favorite_outline,
                      color: Theme.of(context).primaryColorLight,
                    ),
              onPressed: product.toggleFavoriteStatus,
            ),
            // Child will never change and can be passed into builder function
            // child: Text("Never Change"),
          ),
          trailing: IconButton(
            icon: Icon(
              Icons.shopping_cart_outlined,
              color: Theme.of(context).primaryColorLight,
            ),
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              cart.addItem(product.id, product.price, product.title);
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text('Add Item to Cart'),
                  duration: Duration(seconds: 2),
                  action: SnackBarAction(
                    label: 'UNDO',
                    onPressed: () {
                      cart.removeSingleItem(product.id);
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
