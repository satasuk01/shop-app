import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products_provider.dart';
import './product_item.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFavs;
  const ProductsGrid(this.showFavs);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<ProductsProvider>(context);
    final products = showFavs ? productsData.favoriteItems : productsData.items;

    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: products.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        // Certain amount of columns
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10, // Space between row
      ),
      itemBuilder: (ctx, idx) {
        // Product product = products[idx];
        // ChangeNotifierProvider.value is good for single list (reuse object)
        return ChangeNotifierProvider.value(
          value: products[idx],
          child: ProductItem(),
          // child: ProductItem(product.id, product.title, product.imageUrl),
        );
      },
    );
  }
}
