# shop_app

A Flutter Project about e-commerce app. Support both web and app.   

## Function
- User can create and edit their own products
- Cart and Check out Function
- Favorite Items for each users!  


How to run  
`flutter run -d web-server --web-hostname 0.0.0.0 --web-port 80`

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
